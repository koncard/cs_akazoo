package com.example.estelle.cs_akazoo_app.tracks;

import java.util.ArrayList;

public interface TracksView {

    void showTracklist(ArrayList<Track> tracklist);
}
